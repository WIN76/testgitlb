package testp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CercleK {
	public static final File Database=new File("regions-cercles.txt");
	public static void ecrire(File f,String msg) throws IOException{
		BufferedReader br=new BufferedReader(new FileReader(Database));
		FileWriter fw=new FileWriter(f);
		String line;
		try{
			fw.write(msg);
			int i=1;
			while((line=br.readLine())!=null){
				String[] s=line.split("\t");
				if(s.length>1){
					fw.write("insert into cercle values ("+i+++","+s[0]+",'"+s[1]+"');");

				}
			}
			fw.close();
		}catch (IOException io) {
			// TODO: handle exception
			io.printStackTrace();
		}
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		File f=new File("mali.sql");
		String s="create table cercle(id int(3) ,coderegion int(3),cercle varchar(100),constraint pk primary key(id));";
		ecrire(f,s);

	}

}
