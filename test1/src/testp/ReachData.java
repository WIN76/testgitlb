package testp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
/**
 * un programme qui permet d'afficher les contenues d'un fichier en fonction parametre
 * @author SORY
 *
 */
public class ReachData {
	public static final File bd=new File("regions-cercles.txt");
	/**
	 * une methode qui permet de lister les cercle en fonction du code region
	 * @param coderegion
	 * @throws IOException
	 */
	public static void reachByCode(int coderegion) throws IOException{
		BufferedReader br=new BufferedReader(new FileReader(bd));
		String line;
		while((line=br.readLine())!=null){
			String[] tab=line.split("\t");
			if(tab.length>1){
				int c=Integer.parseInt(tab[0]);
				if(c==coderegion){
					System.out.println(tab[1]);
				}
			}
		}
	}

	public static void main(String[] args) throws IOException {
		for(int i=1;i<11;i++){
			if(i==1){
				System.out.println("====kayes====");
				reachByCode(i);
			}
			if(i==2){
				System.out.println("====Koulikoro====");
				reachByCode(i);
			}
			if(i==3){
				System.out.println("====Segou====");
				reachByCode(i);
			}
			if(i==4){
				System.out.println("====Sikasso====");
				reachByCode(i);
			}
			if(i==5){
				System.out.println("====Mopti====");
				reachByCode(i);
			}
			if(i==6){
				System.out.println("====Tombouctou====");
				reachByCode(i);
			}
			if(i==7){
				System.out.println("====Gao====");
				reachByCode(i);
			}
			if(i==8){
				System.out.println("====Kidal====");
				reachByCode(i);
			}
			if(i==9){
				System.out.println("====Inconnu====");
				reachByCode(i);
			}
			if(i==10){
				System.out.println("====Inconnu====");
				reachByCode(i);
			}
		}

	}

}
